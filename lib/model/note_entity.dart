
import 'package:equatable/equatable.dart';

import 'package:hive/hive.dart';

part 'note_entity.g.dart';

@HiveType(typeId: 2)
class Note extends Equatable {
   const Note({
      this.id = '',
      this.content,
      this.timeCreate,
      this.title,
      this.timeUpdate,
      this.isDelete = false,
   });
   @HiveField(0, defaultValue: '')
   final String id;

   @HiveField(1, defaultValue: '')
   final String? title;

   @HiveField(2)
   final String? content;

   @HiveField(3)
   final DateTime? timeCreate;

   @HiveField(4)
    final DateTime? timeUpdate;

   @HiveField(5)
   final bool? isDelete;


   @override
   // TODO: implement props
   List<Object?> get props =>
       [id, timeCreate, content, title, isDelete,timeUpdate];
}
