

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/model/enum.dart';
import 'package:note_app/model/note_entity.dart';
import 'package:note_app/ultils/local_utils.dart';

part 'note_detail_state.dart';

class NoteDetailCubit extends Cubit<NoteDetailState> {
  NoteDetailCubit() : super(NoteDetailState());
  final LocalUtils localUtils = LocalUtils();

  Future <void> updateNote(Note? note) async {
    if (note != null) {
      final updateNote = await localUtils.updateNote(
        Note(
            id: note.id,
            title: state.title,
            content: state.content,
            timeCreate: note.timeCreate
          ),
      );

      if(updateNote != null){
        emit(state.copyWith(loadStatus: LoadStatus.success,noteUpdate:updateNote));
        emit(state.copyWith(loadStatus: LoadStatus.loadingMore));
      }else{
        if (kDebugMode) {
          print('updateNote Fail (Cubit)');
        }
        emit(state.copyWith(loadStatus: LoadStatus.fail));
      }
    }else{
      if (kDebugMode) {
        print("current Note is Null");
      }
      emit(state.copyWith(loadStatus: LoadStatus.fail));
    }
  }

  void changeContent(String value) {
    emit(state.copyWith(content: value));
  }
  void changeTitle(String value) {
    emit(state.copyWith(title: value));
  }
}
