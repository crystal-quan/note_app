
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/app_widget/show_floadting_action.dart';
import 'package:note_app/pages/note_detail_page/note_detail_cubit.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import '../../app_color.dart';
import '../../app_theme.dart';
import '../../app_widget/text_field_custom.dart';
import '../../model/note_entity.dart';

class NoteDetailPage extends StatelessWidget {
  final Note? note;

  const NoteDetailPage({Key? key, required this.note}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => NoteDetailCubit(),
      child: NoteDetailView(note: note),
    );
  }
}

class NoteDetailView extends StatefulWidget {
  final Note? note;

  const NoteDetailView({Key? key, required this.note}) : super(key: key);

  @override
  State<NoteDetailView> createState() => _NoteDetailViewState();
}

class _NoteDetailViewState extends State<NoteDetailView> {
  late NoteDetailCubit _cubit;

  @override
  void initState() {
    _cubit = context.read<NoteDetailCubit>();
    // TODO: implement initState
    super.initState();
    _cubit.changeTitle(widget.note?.title ?? '');
    _cubit.changeContent(widget.note?.content ?? '');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 55, horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldCustom(
              maxLines: 2,
              controller: TextEditingController(text: widget.note?.title),
              onChanged: (p0) => _cubit.changeTitle(p0),
              title: 'Write title here ...',
              hintTextStyle: AppTextStyle.bigTitle),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 18),
              child: Text(
                '${widget.note?.timeUpdate ?? widget.note?.timeCreate}',
                style:
                    const TextStyle(fontSize: 12, color: AppColor.darkPrimary),
              )),
          Expanded(
            child: TextFieldCustom(
              controller: TextEditingController(text: widget.note?.content),
              maxLines: null,
              onChanged: (p0) => _cubit.changeContent(p0),
              title: 'Write content here ...',
              hintTextStyle: const TextStyle(color: AppColor.darkPrimary),
            ),
          ),
          ShowFloatingAction(
            opacity: 1,
            onTap: () {
              _cubit.updateNote(widget.note);
            },
            onTapGoBack: () => Navigator.pop(context),
          ),
          BlocListener<NoteDetailCubit, NoteDetailState>(
            child: const SizedBox(),
            listenWhen: (previous, current) =>
                previous.noteUpdate != current.noteUpdate,
            listener: (context, state) {
              if (state.noteUpdate != null) {
                Navigator.pop(context, state.noteUpdate);
              } else {
                showTopSnackBar(
                  context,
                  const CustomSnackBar.error(
                    message: "Update Note Fail",
                  ),
                );
              }
            },
          )
        ],
      ),
    ));
  }
}
