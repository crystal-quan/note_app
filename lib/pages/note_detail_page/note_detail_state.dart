part of 'note_detail_cubit.dart';

class NoteDetailState {
  NoteDetailState(
      {this.title = '', this.content = '', this.loadStatus = LoadStatus.init,this.noteUpdate});

  final String title;
  final String content;
  final LoadStatus loadStatus;
  final Note? noteUpdate;

  NoteDetailState copyWith(
      {String? title, String? content, LoadStatus? loadStatus,Note? noteUpdate}) {
    return NoteDetailState(
      title: title ?? this.title,
      content: content ?? this.content,
      loadStatus: loadStatus ?? this.loadStatus,
      noteUpdate: noteUpdate ?? this.noteUpdate,
    );
  }
}
