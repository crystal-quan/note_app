part of 'create_note_cubit.dart';

class CreateNoteState {
  CreateNoteState({this.dateTime, this.title ='', this.content = '',this.isShowCheck = false,this.loadStatus = LoadStatus.init,this.note});
  final bool isShowCheck ;
  final String title;
  final String content;
  final DateTime? dateTime;
  final LoadStatus loadStatus;
  final Note? note;

  CreateNoteState copyWith(
      {String? title, String? content, DateTime? dateTime,bool? isShowCheck,LoadStatus? loadStatus,Note? note}) {
    return CreateNoteState(
        title: title ?? this.title,
        content: content ?? this.content,
        dateTime: dateTime ?? this.dateTime,
      isShowCheck: isShowCheck?? this.isShowCheck,
       loadStatus:  loadStatus ?? this.loadStatus,
      note: note ?? this.note,
    );
  }
}
