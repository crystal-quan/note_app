import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/model/enum.dart';
import 'package:note_app/model/note_entity.dart';
import 'package:note_app/ultils/local_utils.dart';

part 'create_note_state.dart';

class CreateNoteCubit extends Cubit<CreateNoteState> {
  CreateNoteCubit() : super(CreateNoteState());
  final LocalUtils localUtils = LocalUtils();

  void changeTitle(String title) {
    emit(state.copyWith(title: title));
  }

  void changeContent(String content) {
    emit(state.copyWith(content: content));
  }

  Future<bool> addNote() async {
    try {
      final note = await localUtils
          .saveNote(Note(id: '', content: state.content, title: state.title));
      if (note != null) {
        emit(
          state.copyWith(
            note: note,
            loadStatus: LoadStatus.success,
          ),
        );
        return true;
      } else {
        emit(state.copyWith(loadStatus: LoadStatus.fail));
        return false;
      }
    } catch (e) {
      if (kDebugMode) {
        print('addNote Fail - (CreateNoteCubit)');
      }
      emit(state.copyWith(loadStatus: LoadStatus.fail));
      return false;
    }
  }

  void showCheck() {
    emit(state.copyWith(isShowCheck: true));
  }
}
