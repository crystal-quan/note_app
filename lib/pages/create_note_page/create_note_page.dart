

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/app_color.dart';
import 'package:note_app/app_theme.dart';
import 'package:note_app/app_widget/show_floadting_action.dart';
import 'package:note_app/app_widget/text_field_custom.dart';
import 'package:note_app/pages/create_note_page/create_note_cubit.dart';

class CreateNotePage extends StatelessWidget {
  const CreateNotePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CreateNoteCubit(),
      child: const CreateNoteView(),
    );
  }
}

class CreateNoteView extends StatefulWidget {
  const CreateNoteView({Key? key}) : super(key: key);

  @override
  State<CreateNoteView> createState() => _CreateNoteViewState();
}

class _CreateNoteViewState extends State<CreateNoteView> {
  late CreateNoteCubit _createNoteCubit;
  @override
  void initState() {
    _createNoteCubit = context.read<CreateNoteCubit>();
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 55, horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldCustom(
              maxLines: 1,
              onChanged: (p0) =>
                  context.read<CreateNoteCubit>().changeTitle(p0),
              title: 'Write title here ...',
              hintTextStyle: AppTextStyle.bigTitleCreateMode),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 18),
              child: Text(
                '${now.day} - ${now.month} - ${now.year}  ${now.hour}:${now.minute}. Zone : ${now.timeZoneName}',
                style: const TextStyle(
                    fontSize: 12, color: AppColor.lightPlaceholder),
              )),
          Expanded(
            child: TextFieldCustom(
              onChanged: (p0) =>
                  context.read<CreateNoteCubit>().changeContent(p0),
              title: 'Write content here ...',
              hintTextStyle: const TextStyle(color: AppColor.lightPlaceholder),
            ),
          ),
          BlocBuilder<CreateNoteCubit, CreateNoteState>(
            buildWhen: (previous, current) =>
            previous.title != current.title,
            builder: (context, state) {
              return ShowFloatingAction(
                onTapGoBack: () => Navigator.pop(context),
                opacity: (state.title == '') ? 0.5 : 1,
                onTap: () async {
                  if(state.title != ''){
                    final result = await _createNoteCubit.addNote();
                    if(result){
                      if(!mounted) return false;
                      Navigator.of(context).pop(_createNoteCubit.state.note);
                    }
                  }else{
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Can not leave the title blank !'),
                      ),
                    );
                  }
                },
              );
            },
          ),
        ],
      ),
    ));
  }
}
