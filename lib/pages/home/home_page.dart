import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/app_color.dart';
import 'package:note_app/app_image.dart';
import 'package:note_app/app_theme.dart';
import 'package:note_app/app_widget/app_bar.dart';
import 'package:note_app/app_widget/bottom_floating_button.dart';
import 'package:note_app/app_widget/show_action.dart';
import 'package:note_app/app_widget/text_fileld_search.dart';
import 'package:note_app/model/enum.dart';
import 'package:note_app/model/note_entity.dart';
import 'package:note_app/pages/create_note_page/create_note_page.dart';
import 'package:note_app/pages/home/home_cubit.dart';
import 'package:note_app/pages/note_detail_page/note_detail_page.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeCubit(),
      child: const HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeCubit _cubit;

  @override
  void initState() {
    _cubit = context.read<HomeCubit>();
    super.initState();
    context.read<HomeCubit>().getAllNote();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 55, horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const AppBarCustom(
              title: 'My Notes', style: AppTextStyle.veryBigTitle),
          const SizedBox(
            height: 36,
          ),
          BlocBuilder<HomeCubit, HomeState>(
            buildWhen: (previous, current) =>
                previous.loadStatus != current.loadStatus,
            builder: (context, state) {
              return TextFieldSearch(
                onChanged: (value) {
                  _cubit.searchTitle(value);
                },
                onTap: () {
                  if (state.value != '' &&
                      (state.listSearch?.isEmpty ?? false)) {
                    showTopSnackBar(
                      context,
                      const CustomSnackBar.error(
                        message: "Result is null",
                      ),
                    );
                  }
                },
              );
            },
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 24),
            child: Text('Note List', style: AppTextStyle.normalTitle),
          ),
          Expanded(
            child: BlocBuilder<HomeCubit, HomeState>(
              buildWhen: (previous, current) =>
                  (previous.loadStatus != current.loadStatus) ||
                  (previous.listSearch != current.listSearch),
              builder: (context, state) {
                var currentListNote =
                    (state.value != '') ? state.listSearch : state.listNote;
                return (currentListNote != null && currentListNote.isNotEmpty)
                    ? ListView.separated(
                        addAutomaticKeepAlives: true,
                        padding: EdgeInsets.zero,
                        shrinkWrap: true,
                        itemCount: currentListNote.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () async => await _navigateToNoteDetail(
                                context, currentListNote[index]),
                            child: Container(
                              decoration: const BoxDecoration(
                                color: AppColor.lightSecondary,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 24),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${currentListNote[index]?.title}',
                                          style: AppTextStyle.smallTile,
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                            '${currentListNote[index]?.content}',
                                            style: AppTextStyle.content,
                                            maxLines: 4,
                                            overflow: TextOverflow.ellipsis),
                                      ],
                                    ),
                                  ),
                                  BlocBuilder<HomeCubit, HomeState>(
                                    buildWhen: (previous, current) =>
                                        previous.isDelete != current.isDelete,
                                    builder: (context, state) {
                                      return state.isDelete
                                          ? ShowAction(
                                              noteStatus: NoteStatus.showDelete,
                                              onTapDelete: () {
                                                _cubit.deleteNote(
                                                    currentListNote[index]
                                                            ?.id ??
                                                        '');
                                              },
                                            )
                                          : const SizedBox();
                                    },
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 18),
                      )
                    : Text(
                        (state.value != '' &&
                                (state.listSearch?.isEmpty ?? false))
                            ? 'Result is null'
                            : 'Add your first note please :D',
                        style: const TextStyle(
                            color: AppColor.redAccent,
                            fontSize: 18,
                            fontWeight: FontWeight.w500),
                      );
              },
            ),
          ),
          BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  state.isDelete
                      ? const SizedBox()
                      : CustomFloatingButton(
                          onTap: () {
                            _cubit.showDeleteNote();
                          },
                          assetName: AppImages.icDelete,
                          bgColor: AppColor.redAccent,
                          padding: const EdgeInsets.all(18),
                        ),
                  const SizedBox(width: 24),
                  state.isDelete
                      ? CustomFloatingButton(
                          assetName: AppImages.icCheck,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 14, vertical: 18),
                          bgColor: AppColor.redAccent,
                          onTap: () {
                            _cubit.showDeleteNote();
                          },
                        )
                      : CustomFloatingButton(
                          onTap: () async {
                            await _navigateAndDisplaySelection(context);
                          },
                          assetName: AppImages.icAdd,
                          bgColor: AppColor.greenAccent,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 18, vertical: 18),
                        )
                ],
              );
            },
          ),
        ],
      ),
    ));
  }

  Future<void> _navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CreateNotePage()),
    );

    if (result is Note) {
      _cubit.addNoteFromNavigator(result);
    }
    if (kDebugMode) {
      print(result);
    }

    if (!mounted) return;
    if (result != null) {
      showTopSnackBar(
        onTap: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
        animationDuration: const Duration(milliseconds: 1000),
        displayDuration: const Duration(milliseconds: 1000),
        context,
        const CustomSnackBar.success(
          message: "Added a note",
        ),
      );
    } else {
      // showTopSnackBar(
      //   onTap: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
      //   animationDuration: Duration(milliseconds: 500),
      //   displayDuration: Duration(milliseconds: 500),
      //   context,
      //   const CustomSnackBar.error(
      //     message: "Add Note Fail",
      //   ),
      // );
    }
  }

  Future<void> _navigateToNoteDetail(BuildContext context, Note? note) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoteDetailPage(note: note)),
    );

    if (result is Note) {
      _cubit.updateFromNavigator(result);
    }
    if (kDebugMode) {
      print(result);
    }

    if (!mounted) return;
    if (result != null) {
      showTopSnackBar(
        onTap: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
        animationDuration: const Duration(milliseconds: 1000),
        displayDuration: const Duration(milliseconds: 1000),
        context,
        const CustomSnackBar.success(
          message: "Update a note complete",
        ),
      );
    } else {
      // showTopSnackBar(
      //   onTap: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
      //   animationDuration: Duration(milliseconds: 500),
      //   displayDuration: Duration(milliseconds: 500),
      //   context,
      //   const CustomSnackBar.error(
      //     message: "Add Note Fail",
      //   ),
      // );
    }
  }
}
