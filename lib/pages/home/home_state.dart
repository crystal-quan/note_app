part of 'home_cubit.dart';

class HomeState {
  const HomeState({this.loadStatus = LoadStatus.init, this.listNoteStatus,this.listNote ,this.isDelete = false,this.listSearch,this.value =''});
  final bool isDelete;
  final LoadStatus loadStatus;
  final List<NoteStatus>? listNoteStatus;
  final List<Note?>? listNote ;
  final List<Note?>? listSearch;
  final String value ;

  HomeState copyWith({LoadStatus? loadStatus, List<NoteStatus>? listNoteStatus,List<Note?>? listNote,bool? isDelete,List<Note?>? listSearch,String? value}) {
    return HomeState(
      listNoteStatus:listNoteStatus?? this.listNoteStatus ,
      loadStatus: loadStatus ?? this.loadStatus,
      listNote: listNote ?? this.listNote,
      isDelete: isDelete ?? this.isDelete,
      listSearch: listSearch ?? this.listSearch,
      value: value ?? this.value,
    );
  }
}
