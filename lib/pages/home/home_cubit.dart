import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/model/enum.dart';
import 'package:note_app/model/note_entity.dart';
import 'package:note_app/ultils/local_utils.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(const HomeState());
  final localUtils = LocalUtils();

  void getAllNote() async {
    emit(state.copyWith(loadStatus: LoadStatus.loading));
    try {
      final allNote = localUtils.getAllNote();
      if (allNote.isNotEmpty) {
        allNote.sort(
          (a, b) => (b?.timeUpdate?? b?.timeCreate!)!
              .compareTo((a?.timeUpdate ?? a!.timeCreate!)),
        );
        emit(state.copyWith(loadStatus: LoadStatus.success, listNote: allNote));
      } else {
        emit(state.copyWith(loadStatus: LoadStatus.fail));
      }
    } catch (e) {
      emit(state.copyWith(loadStatus: LoadStatus.fail));
    }
  }

  void showDeleteNote() {
    bool isDelete = !state.isDelete;
    emit(state.copyWith(isDelete: isDelete));
  }

  void searchTitle(String value) {
    emit(state.copyWith(loadStatus: LoadStatus.loading, value: value));
    List<Note?>? searchNote;
    if (value != '' && state.listNote != null && state.listNote!.isNotEmpty) {
      searchNote = state.listNote
          ?.where((element) => element!.title!.contains(value))
          .toList();
      if (searchNote != null && searchNote.isNotEmpty) {
        searchNote.sort(
          (a, b) => (b?.timeUpdate ?? b?.timeCreate)!
              .compareTo(a?.timeUpdate ??a!.timeCreate!),
        );
      }
      emit(state.copyWith(
          loadStatus: LoadStatus.success, listSearch: searchNote));
    } else {
      emit(state.copyWith(
          loadStatus: LoadStatus.fail, listSearch: state.listNote));
    }
  }

  void deleteNote(String id) async {
   late bool isDelete;
    if (id != '') {
      isDelete = await localUtils.deleteNote(id);
    } else {
      isDelete = false;
    }
    if (isDelete) {
      emit(state.copyWith(loadStatus: LoadStatus.loading));
      final indexDelete =
          state.listNote?.indexWhere((element) => element?.id == id);
      if (indexDelete != null) {
        state.listNote?.removeAt(indexDelete);
      } else {
        if (kDebugMode) {
          print('find index note delete Fail (HomeCubit)');
        }
      }
      emit(state.copyWith(
          loadStatus: LoadStatus.success, listNote: state.listNote));
    } else {
      emit(state.copyWith(loadStatus: LoadStatus.fail));
    }
  }

  void addNoteFromNavigator(Note note) {
    emit(state.copyWith(loadStatus: LoadStatus.loading));
    state.listNote?.insert(0, note);
    emit(
      state.copyWith(
        loadStatus: LoadStatus.success,
        listNote: state.listNote,
      ),
    );
  }
  void updateFromNavigator(Note note){
    emit(state.copyWith(loadStatus: LoadStatus.loading));
   final index = state.listNote?.indexWhere((element) => element?.id == note.id);
   if(index != null){
     state.listNote?.removeAt(index);
     if (kDebugMode) {
       print(index);
     }
     emit(state.copyWith(loadStatus: LoadStatus.loadingMore,listNote: state.listNote));
     state.listNote?.insert(0,note);
   }else{
     if (kDebugMode) {
       print("index is Null (Home Cubit)");
     }
   }
    emit(
      state.copyWith(
        loadStatus: LoadStatus.success,
        listNote: state.listNote,
      ),
    );
  }
}
