import 'dart:developer';
import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:note_app/model/note_entity.dart';

class LocalUtils {
  var box = Hive.box<Note>('notes');

  String getID() {
    const chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    math.Random rnd = math.Random();

    String getRandomString(int length) =>
        String.fromCharCodes(Iterable.generate(
            length, (_) => chars.codeUnitAt(rnd.nextInt(chars.length))));

    String randomId = getRandomString(15);
    if (kDebugMode) {
      print(randomId);
    }
    return randomId;
  }

  Future<Note?> saveNote(Note note) async {
    try {
      final noteID = getID();
      await box.put(
          noteID,
          Note(
              id: noteID,
              isDelete: false,
              title: note.title,
              content: note.content,
              timeCreate: DateTime.now()));
      return Note(
          id: noteID,
          title: note.title,
          content: note.content,
          timeCreate: DateTime.now());
    } catch (e, s) {
      log('saveNote Fail (LocalUtils) - $e ', error: e, stackTrace: s);
      return null;
    }
  }

  List<Note?> getAllNote() {
    final dynamicKeys = box.keys.toList();
    final stringKeys = dynamicKeys.map((e) => e.toString()).toList();
    final listNote = stringKeys.map((e) {
      final oneNote = box.get(e);
      return oneNote;
    }).toList();
    if (kDebugMode) {
      print(stringKeys.last);
    }
    return listNote;
  }

  Future <bool> deleteNote(String id) async {
    try {
      await box.delete(id);
      return true;
    } catch (e, s) {
      log('deleteNote Fail (LocalUtils) - $e', error: e, stackTrace: s);
      return false;
    }
  }

  Future<Note?> updateNote(Note noteUpdate) async {
    try {
      final note = Note(
          id: noteUpdate.id,
          title: noteUpdate.title,
          content: noteUpdate.content,
          timeCreate: noteUpdate.timeCreate,
          timeUpdate: DateTime.now());
      await box.put(
          noteUpdate.id, note
      );
      return note;
    } catch (e, s) {
      log('updateNote Fail (LocalUtils) - $e - $s',error: e ,stackTrace: s);
      return null;
    }
  }
}
