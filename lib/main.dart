import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';


import 'app.dart';
import 'model/note_entity.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(NoteAdapter());

  await Hive.openBox<Note>('notes');
  runApp(const MyApp());
}

