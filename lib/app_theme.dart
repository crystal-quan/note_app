

import 'package:flutter/material.dart';
import 'package:note_app/app_color.dart';

class AppTextStyle{
  static const veryBigTitle = TextStyle(fontSize: 36,fontWeight: FontWeight.w700,color: AppColor.darkPrimary);
  static const bigTitle = TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: AppColor.darkPrimary);
  static const bigTitleCreateMode = TextStyle(fontSize: 24,fontWeight: FontWeight.w700,color: AppColor.lightPlaceholder);
  static const normalTitle = TextStyle(fontSize: 24,fontWeight: FontWeight.w600,color: AppColor.darkPrimary);
  static const smallTile = TextStyle(fontSize: 18,fontWeight: FontWeight.w500,color: AppColor.darkPrimary);
  static const content = TextStyle(fontSize: 14,fontWeight: FontWeight.w400,color: AppColor.darkPrimary);
  
}