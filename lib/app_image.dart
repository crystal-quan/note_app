


class AppImages{

  /// images
  static const icSearch = 'assets/images/ic_search.png';

  /// vector
  static const icDelete = 'assets/images/ic_delete.svg';
  static const icAdd = 'assets/images/ic_add.svg';
  static const icBack = 'assets/images/ic_back.svg';
  static const icSave = 'assets/images/ic_save.svg';
  static const icCheck = 'assets/images/ic_check.svg';
  static const icCancel = 'assets/images/ic_cancel.svg';

}