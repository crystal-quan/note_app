import 'package:flutter/material.dart';
import 'package:note_app/app_theme.dart';

class AppBarCustom extends StatelessWidget {
 final String title;
 final TextStyle? style;
  const AppBarCustom({Key? key,required this.title, this.style}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(title,style: style?? AppTextStyle.bigTitle,);
  }
}
