import 'package:flutter/material.dart';

class TextFieldCustom extends StatelessWidget {
 final void Function(String)? onChanged;
 final int? maxLines ;
 final String? title;
 final TextStyle hintTextStyle;
 final TextEditingController ? controller;
  const TextFieldCustom({Key? key,this.onChanged,this.title,required this.hintTextStyle,this.maxLines,this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller ,
      maxLines: maxLines,
      style: hintTextStyle,
      onChanged:onChanged,
      decoration: InputDecoration(
        hintText: title,
        border: InputBorder.none,
        hintStyle:hintTextStyle ,
      ),
    );
  }
}
