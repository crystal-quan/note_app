import 'package:flutter/material.dart';
import 'package:note_app/app_color.dart';

class TextFieldSearch extends StatelessWidget {
  final GestureTapCallback onTap;
 final Function(String)? onChanged;
  const TextFieldSearch({Key? key,required this.onTap,this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20,top: 3),
      decoration: const BoxDecoration(
        color: AppColor.lightSecondary,
        borderRadius: BorderRadius.all( Radius.circular(8)),
      ),
      child: TextField(
        onChanged: onChanged,
        keyboardType: TextInputType.text,
        cursorColor: AppColor.darkPrimary,
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
            border: InputBorder.none,
            suffixIconColor: AppColor.darkPrimary,
            suffixIcon: InkWell(
              onTap: onTap,
                child: Image.asset('assets/images/ic_search.png',
                    color: AppColor.darkPrimary)),
            focusColor: AppColor.darkPrimary,
            fillColor: AppColor.darkPrimary,
            hoverColor: AppColor.darkPrimary,
            hintText: ('Search your note’s title here ...'),
            labelStyle: const TextStyle(
              color: AppColor.lightPlaceholder,
            )),
      ),
    );
  }
}
