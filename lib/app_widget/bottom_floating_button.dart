import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomFloatingButton extends StatelessWidget {
  final Color? bgColor;
  final EdgeInsets? padding;
  final BorderRadiusGeometry? borderRadius;
  final String assetName;
  final void Function()? onTap;
  const CustomFloatingButton({Key? key, this.bgColor, this.padding, required this.assetName, this.onTap, this.borderRadius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: padding,
          decoration: BoxDecoration(
            borderRadius:borderRadius ?? const BorderRadius.all(Radius.circular(500)),
            color: bgColor,
          ),
        child: SvgPicture.asset(
            assetName,fit: BoxFit.contain),
      ),
    );
  }
}
