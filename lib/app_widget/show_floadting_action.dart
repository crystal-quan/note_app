import 'package:flutter/material.dart';
import 'package:note_app/app_image.dart';
import 'package:note_app/app_widget/bottom_floating_button.dart';

import '../app_color.dart';

class ShowFloatingAction extends StatefulWidget {
  final double opacity;
  final Function()? onTap;
  final void Function()? onTapGoBack;
  const ShowFloatingAction({Key? key, required this.opacity, this.onTap, this.onTapGoBack})
      : super(key: key);

  @override
  State<ShowFloatingAction> createState() => _ShowFloatingActionState();
}

class _ShowFloatingActionState extends State<ShowFloatingAction> {
  bool showCheck = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CustomFloatingButton(
          assetName: AppImages.icBack,
          bgColor: AppColor.darkPrimary,
          padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 16),
          onTap: widget.onTapGoBack,
        ),
         const Spacer(),
        showCheck
            ? CustomFloatingButton(
                assetName: AppImages.icCancel,
                bgColor: AppColor.greenAccent,
                padding: const EdgeInsets.all(18),
                onTap: () {
                  setState(() {
                    showCheck = false;
                  });
                },
              )
            : const SizedBox(),
        const SizedBox(width: 24),
        showCheck
            ? CustomFloatingButton(
                assetName: AppImages.icCheck,
                padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 14),
                bgColor: AppColor.redAccent,
                onTap: widget.onTap,
              )
            : CustomFloatingButton(
                assetName: AppImages.icSave,
                bgColor: AppColor.greenAccent.withOpacity((widget.opacity)),
                padding: const EdgeInsets.all(18),
                onTap: () {
                  if(widget.opacity == 1){
                    setState(() {
                      showCheck = true;
                    });
                  }
                },
              ),
      ],
    );
  }
}
