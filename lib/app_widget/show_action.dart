// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:note_app/model/enum.dart';

import '../app_color.dart';
import '../app_image.dart';
import 'bottom_floating_button.dart';

class ShowAction extends StatefulWidget {
  NoteStatus noteStatus ;
  void Function()? onTapDelete ;
  ShowAction({Key? key,required this.noteStatus, this.onTapDelete}) : super(key: key);

  @override
  State<ShowAction> createState() => _ShowActionState();
}

class _ShowActionState extends State<ShowAction> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        (widget.noteStatus == NoteStatus.showDelete)
            ? Row(
          children: [
            Expanded(
                child: CustomFloatingButton(
                  onTap: () {
                    setState(() {
                      widget.noteStatus = NoteStatus.showCheck;
                    });
                  },
                  assetName: AppImages.icDelete,
                  padding: const EdgeInsets.symmetric(
                    vertical: 11,
                  ),
                  bgColor: AppColor.redAccent,
                  borderRadius: const BorderRadius.vertical(
                      bottom: Radius.circular(8)),
                ))
          ],
        )
            : const SizedBox(),
        (widget.noteStatus == NoteStatus.showCheck)
            ? Row(
          children: [
            Expanded(
              child: CustomFloatingButton(
                onTap: () {
                  setState(() {
                    widget.noteStatus = NoteStatus.showDelete;
                  });
                },
                assetName: AppImages.icCancel,
                padding: const EdgeInsets.symmetric(
                  vertical: 11,
                ),
                bgColor: AppColor.greenAccent,
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(8)),
              ),
            ),
            Expanded(
              child: CustomFloatingButton(
                onTap: widget.onTapDelete ,
                assetName: AppImages.icCheck,
                padding: const EdgeInsets.symmetric(
                  vertical: 11,
                ),
                bgColor: AppColor.redAccent,
                borderRadius:const BorderRadius.only(
                    bottomRight: Radius.circular(8)),
              ),
            ),
          ],
        )
            : const SizedBox(),
      ],
    );
  }
}
